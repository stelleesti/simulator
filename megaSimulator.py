import requests
import json
import schedule
import time
import threading
from http.server import BaseHTTPRequestHandler, HTTPServer
import random
import multiprocessing
from datetime import datetime
baseUrl = "https://test.stell.ee/api/"
token = ""
session = requests.Session()

evsCategories = []
properties = []
technicians = []
taskNr = 0
orderNr = 0
lipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sed magna in nibh elementum vulputate id ullamcorper mi. Aliquam sit amet convallis ligula. Ut massa lacus, maximus sit amet eros at, ultrices semper orci."
logFileName = "stell-log.txt"


def logMessage(message):
    global logFileName
    with open(logFileName, 'a') as f:
        f.write("\n" + message)
    f.close()

def makeLogin(email, passWord):
    return {"email" : email, "password": passWord }

def login(email, passWord):
    global token
    logMessage("logging in as " + email)
    parameters =  json.dumps(makeLogin(email, passWord))
    x = session.post(baseUrl + "user/login", data = parameters)
    token = json.loads(x.text)['authToken']

def makeHeader(token):
    return {"Authorization": "Bearer " + token, 'Content-type':'application/json', 'Accept':'application/json' }

def current_milli_time():
    return round(time.time() * 1000)


def taskActionsJson(taskId, comment, name, dateTime):
    if comment:
        return {"taskId" : taskId, "latitude": "null", "longitude" : "null", "accuracy": "null", "comment": comment }
    elif name:
         return {"taskId" : taskId, "latitude": "null", "longitude" : "null", "accuracy": "null", "name": name, "signature" : "data:image/png;base64,iVBORw0KGg"}
    else:
         return {"taskId" : taskId, "latitude": "null", "longitude" : "null", "accuracy": "null" }

def getData():
    login("planner@stell.ee", "complexpassword123!")
    x = session.get(baseUrl + "user?size=100", headers = makeHeader(token))
    if x.text != "":
        for d in json.loads(x.text)['data']:
            if d['role']['roleName'] == "TECHNICIAN":
                newWorker = {
                    "id" : d['id'],
                    "email" : d['email']
                }
                technicians.append(newWorker)

    evs = session.get(baseUrl + "evs-category", headers = makeHeader(token))
    if evs.text != "":
        print(evs)
        for ev in json.loads(evs.text)['data']:
            evsCategories.append(ev['id'])

    properyResult = session.get(baseUrl + "property", headers = makeHeader(token))
    if properyResult.text != "":
         for prop in json.loads(properyResult.text)['data']:
            properties.append(prop['id'])


def worker(technicians):
    for tech in technicians:
        login(tech['email'], "complexpassword123!")
        activeJob = getWorkerActiveJobObject()
        if activeJob:
            activeJobId = activeJob["id"]
            userSpentTime = activeJob["userSpentTime"]
            currentTime = current_milli_time() + 3600000
            job = session.get(baseUrl + "tech/task/" + activeJobId, headers = makeHeader(token))
            jobStartTime = json.loads(job.text)['data']['estimatedTime']
            randomSpentTime = random.randint(1800000, 3600000)
            if jobStartTime - currentTime < 900000 and userSpentTime == 0:
                taskAction(activeJobId, "start")
            elif userSpentTime > randomSpentTime:
                workerFinish()
        


def checkIfWorkersAreFree():
    for tech in technicians:
        login(tech["email"], "complexpassword123!" )
        activeJobId = getWorkerActiveJobs()
        if not activeJobId:
            return tech["id"]
        else:
            return None

def getWorkerWorkTime():
    x = session.get(baseUrl + "tech/task/active", headers = makeHeader(token))
    if x.text != "":
        workedTime = json.loads(x.text)['data']['userSpentTime']
        return workedTime

def workerFinish():
    x = session.get(baseUrl + "tech/task/active", headers = makeHeader(token))
    randomForErrors = random.randint(1,20)
    activeJobId = getWorkerActiveJobs()
    if activeJobId:
        if randomForErrors == 5:
            taskAction(activeJobId, "obstruction")
        else:
            taskAction(activeJobId, "pause")
            taskAction(activeJobId, "finish")
            taskAction(activeJobId, "complete")
            taskAction(activeJobId, "sign")


def getWorkerActiveJobs():
    x = session.get(baseUrl + "tech/task/active", headers = makeHeader(token))
    if x.text != "":
        return json.loads(x.text)['data']['id']

def getWorkerActiveJobObject():
    x = session.get(baseUrl + "tech/task/active", headers = makeHeader(token))
    if x.text != "":
        return json.loads(x.text)['data']


def scheduleTaskJson(ts, taskId, userId):
    return {"taskId": taskId, "ts": ts, "userId" : userId }

def assignScheduleTask(ts, taskId, freeWorkerId):
    parameters = json.dumps(scheduleTaskJson(ts, taskId, freeWorkerId))
    result = session.post(baseUrl + "plan/order/schedule", data = parameters, headers = makeHeader(token))
    if not result.text.find("error"):
        print(result.text)
        logMessage("assigned task id " + json.loads(result.text)['data']['id'] + " to " + freeWorkerId)


def fillworkerSchedule(workerList, startTime, endTimeS):
    global taskNr
    global orderNr
    login("planner@stell.ee", "complexpassword123!")
    for worker in workerList[:-2]:
        workerId = worker['id']
        start = startTime
        endTime = start + endTimeS
        while start < (endTime):
            randomNr = random.randint(1, 20)
            duration = random.randint(2, 16) * 900000
            taskNr = taskNr + 1
            createdTask = createTask(workerId, duration, "test-task-test-" + str(taskNr))
            if createTask != 0:
                if randomNr == 999:
                    assignScheduleTask(start, createdTask, workerId)
                else:
                    assignTask(orderNr, createdTask, workerId)
                    orderNr = orderNr + 1
                start = start + duration + 900000

def createTaskJson(duration, taskNr):
    return {"description": lipsum, "evsCategoryId": random.choice(evsCategories), "expectedDuration" : duration, "name": taskNr, "priorityId" : "13dd62e2-d5a5-4df9-94e6-298946d14984", "propertyId" : random.choice(properties)}

def createTask(freeWorkerId, duration, taskNr):
    global createdTasks
    parameters = json.dumps(createTaskJson(duration, taskNr))
    result = session.post(baseUrl + "plan/task", data = parameters, headers = makeHeader(token))
    if json.loads(result.text)['data']['id']:
        logMessage("created task with id" + json.loads(result.text)['data']['id'])
        #assignTask(json.loads(result.text)['data']['id'], freeWorkerId)
        return json.loads(result.text)['data']['id']
    else:
        return 0

def assignTaskJson(orderNumber, taskId, userId):
    return {"orderNumber" : orderNumber, "taskId": taskId, "userId" : userId }

def assignTask(orderNr, taskId, freeWorkerId):
    global createdTasks
    global technicians
    parameters = json.dumps(assignTaskJson(orderNr, taskId, freeWorkerId))
    result = session.post(baseUrl + "plan/order/queue", data = parameters, headers = makeHeader(token))
    if not result.text.find("error"):
        logMessage("assigned task id " + json.loads(result.text)['data']['id'] + " to " + freeWorkerId)

def taskAction(taskId, actionName):
    global token
    taskActionJson = ""
    if actionName == "obstruction":
        taskActionJson = taskActionsJson(taskId, lipsum, "", "")
    elif actionName == "sign":
        taskActionJson = taskActionsJson(taskId, "", "kristo", "")
    elif actionName == "complete":
        taskActionJson = taskActionsJson(taskId, lipsum, "", "")
    parameters = json.dumps(taskActionJson)
    result = session.post(baseUrl + "tech/task/" + taskId + "/" + actionName, data = parameters, headers = makeHeader(token))
    logMessage(actionName + " task " + taskId)

scriptStart = current_milli_time()
timeToSkip = 28800000

def getUntilWorkdayEnd():
    hours =  17 - datetime.now().hour
    minutes = 60 - datetime.now().minute
    milliseconds = (hours * 60 * 60 * 1000) + (minutes * 60 * 1000)
    return milliseconds

def plannerScript():
    global scriptStart
    runCount = 0
    print(scriptStart)
    print(scriptStart + timeToSkip * 1)
    while scriptStart < scriptStart + timeToSkip * 1:
        if runCount == 0:
            end = getUntilWorkdayEnd()
            fillworkerSchedule(technicians, scriptStart, end)
            scriptStart = scriptStart + end + 57600000
            runCount +=1
        else:
            print("seal")
            fillworkerSchedule(technicians, scriptStart, timeToSkip)
            scriptStart = scriptStart + timeToSkip
            runCount +=1
            print(scriptStart)

def scriptWorker():
    worker(technicians)

getData()
plannerScript()
scriptWorker()

schedule.every(1).minutes.do(scriptWorker)
#chedule.every(1).minutes.do(plannerScript)

while True:
    schedule.run_pending()
    time.sleep(1)

